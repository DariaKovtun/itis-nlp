import json
import os
import re
import shutil
from functools import reduce
from time import perf_counter
from typing import Tuple


def collect_files(input_dir: str, output_dir: str) -> None:
    file_counter = -1

    def sort_key(name):
        try:
            return int(name.split(".")[0])
        except ValueError:
            return name

    print("Копирование файлов...")
    for dirname, dirs, files in os.walk(input_dir):
        dirs.sort()
        files.sort(key=sort_key)

        if ".DS_Store" in files:
            files.remove(".DS_Store")

        biggest_number = -1
        for file in files:
            old_name = file.split(".")
            if int(old_name[0]) > biggest_number:
                biggest_number = int(old_name[0])
                file_counter += 1
            new_name = f"{file_counter}.{old_name[1]}"
            shutil.copyfile(dirname + "/" + file, output_dir + "/" + new_name)


def find_context(text: str, start: int, end: int) -> str:
    middle_start = None
    middle_end = None

    idx = start
    count = -1  # код считает пробелы, и пробел около сущности не считается

    while middle_start is None:
        if count == 5 or idx <= 0:
            middle_start = max(idx, 0)
            break

        if text[idx - 1] == " ":
            count += 1

        idx -= 1

    idx = end
    count = -1

    while middle_end is None:
        if count == 5 or idx >= len(text):
            middle_end = min(idx, len(text))
            break

        if text[idx] == " ":
            count += 1

        idx += 1

    return text[middle_start:middle_end].strip()


def find_negative_contexts(text: str, rel_dict: dict, ent_dict: dict, doc_id: str) -> list:
    rel_list = [(r["entity1"], r["entity2"]) for r in rel_dict.values()]
    ent_list = sorted(ent_dict.values(), key=lambda e: e["start"])
    result = []

    for i in range(len(ent_list) - 1):
        e_1, e_2 = ent_list[i], ent_list[i+1]

        relation_exists = False
        for rel in rel_list:
            if rel[0]["id"] == e_1["id"] and rel[1]["id"] == e_2["id"]:
                relation_exists = True
                break

        if not relation_exists:
            rel_dict = {
                "doc_id": doc_id,
                "label": 1,
                "entity1": e_1,
                "entity2": e_2,
                "middle_context": text[e_1["end"]:e_2["start"]].strip(),
                "context": find_context(text, e_1["start"], e_2["end"])
            }

            result.append(rel_dict)

    return result


def process_file(filename: str) -> list:
    result = []

    try:
        with open(f"{filename}.ann") as file:
            ann_lines = file.readlines()

        relations = dict()
        entities = dict()

        ent_re = re.compile(r"(?P<id>T\d+)\t(?P<type>\w+)\s(?P<start>\d+)\s(\d+;\d+\s)?(?P<end>\d+)\t(?P<text>.+)$")
        rel_re = re.compile(r"(?P<id>R\d+)\t(?P<type>[A-Za-z_]+)\sArg1:(?P<entity1>T\d+)\sArg2:(?P<entity2>T\d+)")

        for line in ann_lines:
            if line.startswith("T"):
                m_e = ent_re.match(line.strip())
                if m_e:
                    entities[m_e.group("id")] = m_e.groupdict()
                    entities[m_e.group("id")]["start"] = int(entities[m_e.group("id")]["start"])
                    entities[m_e.group("id")]["end"] = int(entities[m_e.group("id")]["end"])
            elif line.startswith("R"):
                m_r = rel_re.match(line.strip())
                if m_r:
                    relations[m_r.group("id")] = m_r.groupdict()

        with open(f"{filename}.txt") as file:
            text = file.read()

        def entity_key(e):
            return int(e["start"])

        for rel_id, rel_data in relations.items():
            rel_data["entity1"], \
            rel_data["entity2"] = min(entities[rel_data["entity1"]], entities[rel_data["entity2"]], key=entity_key), \
                                  max(entities[rel_data["entity1"]], entities[rel_data["entity2"]], key=entity_key)

            rel_data["doc_id"] = filename.split("/")[-1]
            rel_data["label"] = 1
            rel_data["context"] = find_context(text,
                                               rel_data["entity1"]["start"],
                                               rel_data["entity2"]["end"])
            rel_data["middle_context"] = text[
                rel_data["entity1"]["end"]:
                rel_data["entity2"]["start"]
            ].strip()
            result.append(rel_data)

        result.extend(find_negative_contexts(text, relations, entities, filename.split("/")[-1]))

    except FileNotFoundError:
        pass

    return result


def print_statistics(result):
    context_len_avg = 0
    context_len_max = 0
    unique = set()
    unique_middle = set()
    for relation in result:
        context_len_avg += len(relation["context"])
        if len(relation["context"]) > context_len_max:
            context_len_max = len(relation["context"])
        for word in relation["context"].split():
            unique.add(word)
        for word in relation["middle_context"].split():
            unique_middle.add(word)
    context_len_avg /= len(result)

    print("Отношений: %d" % len(result))
    print("Средняя длина контекста: %.2f символов" % context_len_avg)
    print("Максимальная длина контекста: %d символов" % context_len_max)
    print("Уникальных слов в контекстах: %d" % len(unique))
    print("Уникальных слов в промежуточных контекстах: %d" % len(unique_middle))


def process_data(input_dir: str, output_dir: str) -> None:
    print("Обработка файлов...")
    result = []

    for file_num in range((len(os.listdir(input_dir)) + 1) // 2):
        result.extend(process_file(f"{input_dir}/{file_num}"))
        
    print_statistics(result)

    with open(f"{output_dir}/output.json", "w", encoding="utf-8") as f:
        f.write(json.dumps(result, indent=2, ensure_ascii=False))


if __name__ == "__main__":
    print("=== Препроцессинг данных -- русский корпус пациентов ===")
    start = perf_counter()
    # collect_files("input_data", "intermediate_data")
    process_data("intermediate_data", "output_data")
    end = perf_counter()
    print("Готово.")
    print("Время: %.2f с" % (end - start))
