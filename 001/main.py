from functools import reduce
from json import dumps

from lxml import etree


def read_tree_from_xml(filename):
    with open(filename, "rb") as xmldocument:  # байт.чтения требует библиотека
        document1 = xmldocument.read()  # байтовая строка с текстом документа

    return etree.fromstring(document1)  # из строки делаем дерево


def anno_to_entity(anno):
    start = int(anno.xpath("location/@offset")[0])
    length = int(anno.xpath("location/@length")[0])
    end = start + length

    return {
        "id": anno.get("id"),
        "text": anno.xpath("text")[0].text,
        "start": start,
        "end": end,
        "type": anno.xpath("infon[@key='type']")[0].text
    }


def get_entities(doc_root, ids):
    entities = []

    for i in ids:
        entities.append(
            anno_to_entity(
                doc_root.xpath(f"passage/annotation[@id='{i}']")[0]
            )
        )

    return entities


def convert_document(doc_element):
    relations = []

    rel_els = doc_element.xpath("passage/relation")
    entities = sorted(list(map(anno_to_entity, doc_element.xpath("passage/annotation"))), key=lambda e: e["start"])
    doc_text = doc_element.xpath("passage/text")[0].text

    sentences = doc_text.split("\n")
    sentence_ends = []
    buffer = 0

    def contexts(entity1, entity2):
        middle = doc_text[entity1["end"]:entity2["start"]].strip()
        end = reduce(
            lambda a, b: b if entity1["start"] > a else a,
            sentence_ends
        )
        context = sentences[sentence_ends.index(end)]
        return middle, context

    for sentence in sentences:
        buffer += len(sentence)
        sentence_ends.append(buffer)

    for rel in rel_els:
        rel_dict = {
            "id": rel.get("id"),
            "type": rel.xpath("infon[@key='type']")[0].text,
            "doc_id": doc_element.xpath("id")[0].text,
            "label": 1
        }

        entity_ids = rel.xpath("node/@refid")

        rel_dict["entity1"] = next(e for e in entities if e["id"] == entity_ids[0])
        rel_dict["entity2"] = next(e for e in entities if e["id"] == entity_ids[1])

        rel_dict["middle_context"], rel_dict["context"] = contexts(rel_dict["entity1"], rel_dict["entity2"])

        relations.append(rel_dict)

    for i in range(len(entities) - 1):
        e_1, e_2 = entities[i], entities[i+1]

        in_same_sentence = reduce(lambda a, b: b if e_1["start"] > a else a,sentence_ends) == reduce(lambda a, b: b if e_1["start"] > a else a,sentence_ends)
        relation_exists = False
        for rel in relations:
            if rel["entity1"]["id"] == e_1["id"] and rel["entity2"]["id"] == e_2["id"]:
                relation_exists = True
                break

        if in_same_sentence and not relation_exists:
            rel_dict = {
                "doc_id": doc_element.xpath("id")[0].text,
                "label": 0,
                "entity1": e_1,
                "entity2": e_2,
                "middle_context": contexts(e_1, e_2)[0],
                "context": contexts(e_1, e_2)[1]
            }

            relations.append(rel_dict)

    return relations


def extract_relations(root):
    documents = root.xpath("/collection/document")
    documents = list(map(convert_document, documents))
    return [item for lst in documents for item in lst]


def write_obj_to_json(obj, filename):
    json_str = dumps(obj, indent=2)
    with open(filename, "w") as file:
        file.write(json_str)


def print_statistics(result):
    context_len_avg = 0
    context_len_max = 0
    unique = set()
    unique_middle = set()
    for relation in result:
        context_len_avg += len(relation["context"])
        if len(relation["context"]) > context_len_max:
            context_len_max = len(relation["context"])
        for word in relation["context"].split():
            unique.add(word)
        for word in relation["middle_context"].split():
            unique_middle.add(word)
    context_len_avg /= len(result)

    print("Отношений: %d" % len(result))
    print("Средняя длина контекста: %.2f символов" % context_len_avg)
    print("Максимальная длина контекста: %d символов" % context_len_max)
    print("Уникальных слов в контекстах: %d" % len(unique))
    print("Уникальных слов в промежуточных контекстах: %d" % len(unique_middle))


if __name__ == "__main__":
    filename = "hprd50_bioc.xml"
    tree_root = read_tree_from_xml(filename)
    result = extract_relations(tree_root)
    write_obj_to_json(result, filename + ".json")
    print_statistics(result)
