import random
from random import shuffle

import numpy as np

from classify.features import get_features


def prepare_data(relations: list, ent_types: list) -> list:
    x_y = []

    for rel in relations:
        x_y.append((
            get_features(rel, ent_types),
            ("+" if rel['label'] == 1 else "-") + rel['type']
        ))

    return x_y


def prepare_data_dict(relations: list,
                      split: float,
                      classes: list,
                      ent_types: list) -> dict:

    dataset = prepare_dataset(relations)

    x_y = prepare_data(dataset, ent_types)

    _result = {}

    for cls in classes:
        x = []
        y = []

        for rel in x_y:
            lbl = rel[1]

            if lbl[1:] == cls:
                x.append(rel[0])
                if lbl[0] == '+':
                    y.append(f'{cls}')
                else:
                    y.append(f'Neg {cls}')

        split_idx = int(split * len(x))

        x_train = x[:split_idx]
        x_test = x[split_idx:]
        y_train = y[:split_idx]
        y_test = y[split_idx:]

        _result[cls] = {
            'x_train': np.array(x_train),
            'y_train': np.array(y_train),
            'x_test': np.array(x_test),
            'y_test': np.array(y_test)
        }
    return _result


def prepare_dataset(relations: list) -> list:
    shuffled = [*relations]

    random.shuffle(shuffled)

    positive, negative = [], []

    for rel in shuffled:
        if rel['label'] == 1:
            positive.append(rel)
        else:
            negative.append(rel)

    del shuffled

    uniform = []
    quot = len(positive) / len(negative)

    while len(positive) + len(negative) > 0 and len(negative) > 0:
        if len(positive) / len(negative) >= quot:
            uniform.append(positive.pop())
        else:
            uniform.append(negative.pop())

    while len(positive) > 0:
        uniform.append(positive.pop())

    return uniform
