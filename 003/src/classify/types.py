def find_all_rel_types(relations: list) -> list:
    _result = set()

    for rel in relations:
        _result.add(rel['type'])

    return list(_result)


def find_all_ent_types(relations: list) -> list:
    _result = set()

    for rel in relations:
        _result.add(rel['entity1']['type'])
        _result.add(rel['entity2']['type'])

    return list(_result)
