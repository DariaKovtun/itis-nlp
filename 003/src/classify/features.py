from functools import reduce

import numpy as np

from text.embedding import average_vector
from text.vectorize import vectorize


def get_features(rel: dict, ent_types: list) -> list:
    np.seterr('raise')
    return [
        word_dist(rel),
        char_dist(rel),
        sent_dist(rel),

        *bow(rel),
        *types(rel, ent_types),

        *ent_emb(rel),
        *similarity(rel)
    ]


def word_dist(rel: dict) -> int:
    return len(rel['middle_context'])


def char_dist(rel: dict) -> int:
    return reduce(lambda acc, cur: acc + len(cur), rel['middle_context'], 0)


def sent_dist(rel: dict) -> int:
    return reduce(lambda acc, cur: acc + 1 if cur == '.' else acc,
                  rel['middle_context'], 0)


def position(rel: dict) -> int:
    return 0  # FIXME


def bow(rel: dict) -> list:
    return vectorize(' '.join(rel['context']))


def types(rel: dict, ent_types: list) -> list:
    return list(map(
        lambda c:
            1
            if c in [rel['entity1']['type'],
                     rel['entity2']['type']]
            else 0,
        ent_types))


def ent_emb(rel: dict) -> list:
    return [*average_vector(rel['entity1']['text']),
            *average_vector(rel['entity2']['text'])]


def similarity(rel: dict) -> list:
    ent_1, ent_2 = np.array(average_vector(rel['entity1']['text'])), \
                   np.array(average_vector(rel['entity2']['text']))

    taxicab = abs(ent_1 - ent_2).sum()
    euclidian = ((ent_1 - ent_2) ** 2).sum() ** 0.5
    try:
        cosine = 1 - ((ent_1 * ent_2).sum() / (
                         ((ent_1 ** 2).sum() ** 0.5)
                         * ((ent_2 ** 2).sum() ** 0.5)))
    except FloatingPointError:
        cosine = 0

    return [taxicab, euclidian, cosine]
