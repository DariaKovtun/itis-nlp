import os
from pathlib import Path


def walk_all_files(input_dir: str):
    for file_num in range(((len(os.listdir(input_dir)) + 1) // 2) + 1):
        if Path(f'{input_dir}/{file_num}.txt').is_file() \
                and Path(f'{input_dir}/{file_num}.ann').is_file():
            with open(f'{input_dir}/{file_num}.ann') as ann_file:
                ann_text = ann_file.read()

            with open(f"{input_dir}/{file_num}.txt") as txt_file:
                txt_text = txt_file.read()

            yield file_num, ann_text, txt_text
