import tarfile
import zipfile

from utils.log import Loggy

FORMATS = {
    'tgz': lambda src: tarfile.open(src, 'r:gz'),
    'zip': lambda src: zipfile.ZipFile(src, 'r')
}


def unzip(src: str, dest: str, format: str):
    try:
        with FORMATS[format](src) as file:
            file.extractall(dest)
        Loggy().info('Распаковка прошла успешно')
    except:
        Loggy().err('Произошла ошибка')
        exit(2)
