import os
import shutil

from utils.log import Loggy


def merge_dirs(input_dir: str, output_dir: str):
    if not os.path.exists(output_dir):
        Loggy().info("Создание несуществующих директорий...")
        os.makedirs(output_dir)

    file_counter = -1

    def numbered_name_sort(name: str):
        try:
            return int(name.split(".")[0])
        except ValueError:
            return name

    for dirname, dirs, files in os.walk(input_dir):
        dirs.sort()
        files.sort(key=numbered_name_sort)

        if ".DS_Store" in files:
            files.remove(".DS_Store")

        max_number = -1
        for file in files:
            old_name = file.split(".")
            if int(old_name[0]) > max_number:
                max_number = int(old_name[0])
                file_counter += 1
            new_name = f"{file_counter}.{old_name[1]}"

            shutil.copyfile(f"{dirname}/{file}",
                            f"{output_dir}/{new_name}")
