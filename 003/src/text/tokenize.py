import nltk

from utils.log import Loggy

nltk.download('punkt', quiet=True)


def tokenize(text: str) -> list:
    if nltk.download('punkt', quiet=True):
        tokens = nltk.word_tokenize(text, language='russian')
        return [token.lower() for token in tokens]
    else:
        Loggy().err('Ошибка при скачивании токенизатора Punkt')
        exit(3)
