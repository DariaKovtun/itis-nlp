from sklearn.feature_extraction.text import TfidfVectorizer, CountVectorizer

TFIDF_VECTORIZER = TfidfVectorizer(max_features=500)
COUNT_VECTORIZER = CountVectorizer(max_features=500)

VECTORIZER = TFIDF_VECTORIZER  # переключатель векторизаторов


def train_vectorizer(texts: list) -> None:
    VECTORIZER.fit(texts)


def vectorize(sentence: str) -> list:
    return VECTORIZER.transform([sentence]).toarray()[0].tolist()
