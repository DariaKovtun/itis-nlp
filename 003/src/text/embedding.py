from functools import reduce

import gensim
import numpy as np

W2V_MODEL = None


def load_word2vec_model(filename):
    global W2V_MODEL
    W2V_MODEL = gensim.models.KeyedVectors.load_word2vec_format(filename,
                                                                binary=True)


def average_vector(words: list) -> list:
    def word_map(word):
        return W2V_MODEL[word] if word in W2V_MODEL \
            else np.zeros(W2V_MODEL.vector_size)

    vectors = list(map(word_map, words))
    return ((reduce(lambda acc, cur: acc + cur,
                    vectors,
                    np.zeros(W2V_MODEL.vector_size))) / len(vectors)).tolist()
