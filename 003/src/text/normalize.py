import re

import pymorphy2

from text.tokenize import tokenize

MORPH = pymorphy2.MorphAnalyzer()


def normalize(text: str) -> list:
    return list(map(normalize_word, tokenize(text)))


def normalize_word(word: str) -> str:
    parsed = MORPH.parse(word)[0]
    pat = re.compile(r"\W+")
    return f'{parsed.normalized.word}_{opencorpora_to_upos(re.split(pat, str(parsed.normalized.tag))[0])}'


def opencorpora_to_upos(tag: str) -> str:
    return {
        'NOUN': 'NOUN',
        'ADJF': 'ADJ',
        'ADJS': 'ADJ',
        'COMP': 'ADV',
        'VERB': 'VERB',
        'INFN': 'VERB',
        'PRTF': 'VERB',
        'PRTS': 'VERB',
        'GRND': 'VERB',
        'NUMR': 'NUM',
        'ADVB': 'ADV',
        'NPRO': 'PRON',
        'PRED': 'ADV',
        'PREP': 'ADP',
        'CONJ': 'CCONJ',
        'PRCL': 'PART',
        'INTJ': 'INTJ',
        'LATN': 'X',
        'PNCT': 'PUNCT',
        'NUMB': 'NUM',
        'intg': 'NUM',
        'real': 'NUM',
        'ROMN': 'NUM',
        'UNKN': 'X',
        None: 'X'
    }[tag]
