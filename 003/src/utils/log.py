import click


class Loggy:
    ACTION_COUNTER = 0

    def action_str(self) -> str:
        return f'[{str(self.ACTION_COUNTER).zfill(3)}]'

    def log(self, text: str, color: str = None, action: bool = False):
        click.secho(
            f'{self.action_str() if action else " " * len(self.action_str())} '
            f'{text}',
            fg=color)

    def action(self, text: str):
        click.echo()
        self.ACTION_COUNTER += 1
        self.log(text, action=True)

    def info(self, text: str):
        self.log(text)

    def warn(self, text: str, ):
        self.log(text, 'bright_yellow')

    def err(self, text: str):
        self.log(text, 'bright_red')

    def success(self, text: str):
        self.log(text, 'bright_green')
