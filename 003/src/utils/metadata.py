import click

TITLE = \
    "Извлечение отношений из клинических карточек пациентов на русском языке"

_AUTHORS = [
    "Дарья Ковтун",
    "Никита Карамов"
]

AUTHORS_STR = click.style('Авторы', bold=True) + ', '.join(_AUTHORS)
