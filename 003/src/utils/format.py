import click


def put_in_frame(text: str) -> str:
    result = ''

    try:
        width = min(click.get_terminal_size()[0], 80)
    except OSError:
        width = 80

    result += '\u2554' + '\u2550' * (width - 2) + '\u2557' + '\n'

    for line in text.split('\n'):
        result += '\u2551' + line.center(width - 2) + '\u2551' + '\n'

    result += '\u255a' + '\u2550' * (width - 2) + '\u255d'

    return result
