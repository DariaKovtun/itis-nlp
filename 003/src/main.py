#!/usr/bin/env python3
import json
import os
import shutil
import time

import click
import joblib
from sklearn.ensemble import RandomForestClassifier

from classify.prepare import prepare_data_dict
from classify.types import find_all_rel_types, find_all_ent_types
from files.archive import FORMATS, unzip
from files.files import walk_all_files
from files.merge import merge_dirs
from preprocess.process import process_dir
from text.embedding import load_word2vec_model
from text.vectorize import train_vectorizer
from utils.format import put_in_frame
from utils.log import Loggy
from utils.metadata import TITLE

LOG = Loggy()


@click.group()
@click.help_option('-h', '--help')
def __cli():
    click.secho(put_in_frame(TITLE), fg='bright_cyan')


@__cli.command()
@click.option('-i', '--input',
              type=click.Path(exists=True,
                              dir_okay=False,
                              resolve_path=True),
              default=f'{os.path.dirname(__file__)}/../res/input.tar.gz',
              show_default=True,
              help='входные данные')
@click.option('-f', '--format',
              type=click.Choice(choices=list(FORMATS.keys())),
              default=list(FORMATS.keys())[0],
              show_default=True,
              help='формат входных данных')
@click.option('-o', '--output',
              type=click.Path(exists=True,
                              file_okay=False,
                              writable=True,
                              resolve_path=True),
              default=f'{os.path.dirname(__file__)}/../out/',
              show_default=True,
              help='путь к папке для выходных данных')
@click.option('-t', '--temp',
              type=click.Path(exists=False,
                              file_okay=False,
                              writable=True,
                              resolve_path=True),
              default=f'{os.path.dirname(__file__)}/../.tmp',
              show_default=True,
              help='путь к временной папке')
def preprocess(input, format, output, temp):
    start = time.perf_counter()

    LOG.action('Подготовка среды...')
    try:
        os.mkdir(temp)
    except FileExistsError:
        LOG.err('Временная папка уже существует.')
        LOG.err(f'Удалите или переименуйте папку {temp} перед запуском.')
        exit(1)

    LOG.action('Распаковка файлов...')
    unzip(str(input),
          f'{temp}/input',
          format)

    LOG.action('Компоновка входных данных...')
    merge_dirs(f'{temp}/input',
               f'{temp}/intermediate')

    LOG.action('Препроцессинг данных...')
    relations = process_dir(f'{temp}/intermediate')

    LOG.action('Сохранение обработанных данных в файл...')
    with open(f"{output}/relations.json", "w", encoding="utf-8") as f:
        f.write(json.dumps(relations, ensure_ascii=False))

    LOG.action('Подготовка текстов для векторизатора...')
    vectorizer_texts = [txt for _, _, txt in
                        walk_all_files(f'{temp}/intermediate')]
    with open(f"{output}/texts.json", "w", encoding="utf-8") as f:
        f.write(json.dumps(vectorizer_texts, ensure_ascii=False))

    LOG.action('Удаление временных файлов...')
    shutil.rmtree(temp)

    end = time.perf_counter()
    LOG.action(click.style('Готово!', fg='bright_green', bold=True))
    LOG.info(f'Время: {(end - start):.2f} с')


@__cli.command()
@click.option('-o', '--output',
              type=click.Path(exists=True,
                              file_okay=False,
                              writable=True,
                              resolve_path=True),
              default=f'{os.path.dirname(__file__)}/../out/',
              show_default=True,
              help='путь к папке с выходными данными этапа preprocess')
@click.option('-w', '--word2vec',
              type=click.Path(exists=False,
                              dir_okay=False,
                              resolve_path=True),
              default=f'{os.path.dirname(__file__)}/../res/word2vec/model.bin',
              show_default=True,
              help='путь к модели word2vec')
def train(output, word2vec):
    start = time.perf_counter()

    with open(f"{output}/relations.json", "r", encoding="utf-8") as f:
        relations = json.loads(f.read())

    print(f"{len(relations)} relations")

    with open(f"{output}/texts.json", "r", encoding="utf-8") as f:
        texts = json.loads(f.read())

    LOG.action('Настройка векторизатора...')
    train_vectorizer(texts)

    LOG.action('Настройка word2vec...')
    load_word2vec_model(word2vec)

    LOG.info('Выделение возможных типов сущностей...')
    ent_types = find_all_ent_types(relations)
    LOG.info('Выделение возможных типов отношений...')
    rel_types = find_all_rel_types(relations)

    LOG.action('Подготовка данных...')
    classification_data = prepare_data_dict(
        relations,
        .8,
        rel_types,
        ent_types
    )

    LOG.action('Настройка классификаторов...')
    LOG.info('Создание объектов классификаторов...')
    for cls, data in classification_data.items():
        data['clsfr'] = RandomForestClassifier(n_estimators=100,
                                               class_weight={
                                                   cls: 0.7,
                                                   f'Neg {cls}': 0.3
                                               })

    LOG.action('Тренировка классификаторов...')
    for cls, data in classification_data.items():
        data['clsfr'] = data['clsfr'].fit(data['x_train'], data['y_train'])

    LOG.action('Вычисление F-measure...')
    for cls, data in classification_data.items():
        data['score'] = data['clsfr'].score(data['x_test'], data['y_test'])
    avg_score = 0.0
    for cls, data in classification_data.items():
        LOG.info(f"{cls}: {data['score']:.3f}")
        avg_score += data['score']

        # click.secho(sklearn.metrics.classification_report(
        #     data['y_test'],
        #     data['clsfr'].predict(data['x_test'])
        # ), fg='bright_black')

    avg_score /= len(classification_data.keys())
    click.echo()
    LOG.info(f'Средняя F-measure: {avg_score:.3f}')

    LOG.action('Сохранение классификаторов...')
    for cls, data in classification_data.items():
        joblib.dump(data['clsfr'],
                    f'{output}/classifiers/{cls}.pkl.gz',
                    compress=5)

    end = time.perf_counter()
    LOG.action(click.style('Готово!', fg='bright_green', bold=True))
    LOG.info(f'Время: {(end - start):.2f} с')


if __name__ == "__main__":
    __cli()
