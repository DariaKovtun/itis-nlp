from files.files import walk_all_files
from preprocess.extract import extract_annotations
from preprocess.relations import find_positive_relations, \
    find_negative_relations
from utils.log import Loggy


def parse_relations(ann_text: str, txt_text: str, file_num: int) -> list:
    Loggy().info(f'Обработка файла {file_num}')

    _result = []

    ann_lines = ann_text.split('\n')

    entities, relations = extract_annotations(ann_lines)

    _result.extend(find_positive_relations(entities,
                                           relations,
                                           txt_text,
                                           file_num))
    _result.extend(find_negative_relations(entities,
                                           relations,
                                           txt_text,
                                           file_num))

    return _result


def process_dir(input_dir: str) -> list:
    _result = []

    for file_num, ann, txt in walk_all_files(input_dir):
        _result.extend(parse_relations(ann, txt, file_num))

    Loggy().info('Препроцессинг файлов завершён.')
    return _result
