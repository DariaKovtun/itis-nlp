def get_context(text: str,
                sorted_entities: [dict, dict],
                window: int = 5) -> str:
    start = end = None

    idx = sorted_entities[0]['start']
    count = -1  # код считает пробелы, и пробел около сущности не считается
    while start is None:
        if count == window or idx <= 0:
            start = max(idx, 0)
            break

        if text[idx - 1] == " ":
            count += 1

        idx -= 1

    idx = sorted_entities[1]['end']
    count = -1
    while end is None:
        if count == window or idx >= len(text):
            end = min(idx, len(text))
            break

        if text[idx] == " ":
            count += 1

        idx += 1

    return text[start:end].strip().replace('\n', ' ')


def get_middle_context(text: str, sorted_entities: [dict, dict]) -> str:
    return text[
           sorted_entities[0]['end']
           :
           sorted_entities[1]['start']
           ].strip().replace('\n', ' ')
