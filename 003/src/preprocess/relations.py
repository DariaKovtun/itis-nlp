import random

from preprocess.contexts import get_middle_context, get_context
from text.normalize import normalize


def sorted_entities(entity_1, entity_2) -> list:
    return sorted([entity_1, entity_2], key=lambda e: int(e['start']))


def find_positive_relations(entities: dict,
                            relations: dict,
                            text: str,
                            file_num: int) -> list:
    _result = []

    for rel_id, rel_data in relations.items():
        entities_sorted = sorted_entities(entities[rel_data["entity1"]],
                                          entities[rel_data["entity2"]])

        _result.append({
            'entity1': entities_sorted[0],
            'entity2': entities_sorted[1],
            'type': rel_data['type'],
            'doc_id': str(file_num),
            'label': 1,
            'context': normalize(get_context(text, entities_sorted, 10)),
            'middle_context': normalize(get_middle_context(text,
                                                           entities_sorted))
        })

    return _result


def find_negative_relations(entities: dict,
                            relations: dict,
                            text: str,
                            file_num: int) -> list:
    rel_list = [(r['entity1'], r['entity2'])
                for r in relations.values()]

    ent_list = sorted(entities.values(), key=lambda e: e['start'])

    _result = []

    while len(_result) < min(20, int(0.5*len(ent_list)*(len(ent_list)-1))):
        ent_1 = random.choice(ent_list)
        ent_2 = random.choice(ent_list)

        if (ent_1["id"], ent_2["id"]) in rel_list:
            continue

        rel_type = None

        if 'Body_location' in [ent_1['type'], ent_2['type']] \
                and 'Symptom' in [ent_1['type'], ent_2['type']]:
            rel_type = 'Symptom_bdyloc'
        elif 'Course' in [ent_1['type'], ent_2['type']]:
            rel_type = 'Course'
        elif 'Severity' in [ent_1['type'], ent_2['type']]:
            rel_type = 'Severity'
        elif 'Body_location' in [ent_1['type'], ent_2['type']]:
            rel_type = 'Body_location'

        if rel_type is not None:
            _result.append({
                "entity1": ent_1,
                "entity2": ent_2,
                "type": rel_type,
                "doc_id": str(file_num),
                "label": 0,
                "context": normalize(get_context(text, [ent_1, ent_2], 10)),
                "middle_context": normalize(
                    get_middle_context(text, [ent_1, ent_2]))
            })

    return _result
