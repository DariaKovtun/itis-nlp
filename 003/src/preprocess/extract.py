import re

from text.normalize import normalize

RELATION_RE = re.compile(r"^(?P<id>R\d+)\t"
                         r"(?P<type>[A-Za-z_]+)_rel\s"
                         r"Arg1:(?P<entity1>T\d+)\s"
                         r"Arg2:(?P<entity2>T\d+)")

ENTITY_RE = re.compile(r"^(?P<id>T\d+)\t"
                       r"(?P<type>\w+)\s"
                       r"(?P<start>\d+)\s"
                       r"(\d+;\d+\s)?"
                       r"(?P<end>\d+)\t"
                       r"(?P<text>.+)$")


def extract_annotations(ann_lines: list) -> (dict, dict):
    relations = {}
    entities = {}

    for line in ann_lines:
        if e_mat := ENTITY_RE.match(line.strip()):
            entities[e_mat.group("id")] = {
                **e_mat.groupdict(),
                'start': int(e_mat.groupdict()["start"]),
                'end': int(e_mat.groupdict()["end"]),
                'text': normalize(e_mat.groupdict()['text'])
            }

        elif r_mat := RELATION_RE.match(line.strip()):
            relations[r_mat.group("id")] = r_mat.groupdict()

    return entities, relations
